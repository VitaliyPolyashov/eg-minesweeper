export const getKey = (row, column) => {
  return `${row}|${column}`;
};

export const getNeighbors = (data, row, column) => {
  const rowCount = data.length;
  const columnCount = data[0].length;
  const result = [
    [row - 1, column - 1],
    [row - 1, column + 1],
    [row, column - 1],
    [row - 1, column],
    [row + 1, column + 1],
    [row, column + 1],
    [row + 1, column],
    [row + 1, column - 1],
  ];

  return result.filter(
    (cell) =>
      cell[0] >= 0 &&
      cell[0] < rowCount &&
      cell[1] >= 0 &&
      cell[1] < columnCount
  );
};

export const getHiddenNeighbors = (data, row, column) => {
  return getNeighbors(data, row, column).filter(
    (cell) => data[cell[0]][cell[1]] === null
  );
};
