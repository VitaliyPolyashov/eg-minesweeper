import { action, computed, observable, reaction, values } from "mobx";
import Cell from "./Cell";

export default class Sweeper {
  @observable decisionMadeSignal = null;
  @observable isDecisionMakingStopped = null;

  constructor(session) {
    this.session = session;
    reaction(() => this.session.isStarted, this.startMakingDecision);
    reaction(() => this.session.isLosed, this.stopMakingDecision);
    reaction(() => this.session.isWon, this.stopMakingDecision);
  }

  @computed
  get data() {
    const map = observable.map({});
    for (let row = 0; row < this.rowCount; row++) {
      for (let column = 0; column < this.columnCount; column++) {
        const cell = new Cell(this.session.data, row, column);
        map.set(cell.key, cell);
      }
    }
    return map;
  }

  @computed
  get dataValues() {
    return values(this.data);
  }

  @computed
  get rowCount() {
    return this.session.data && this.session.data.length;
  }

  @computed
  get columnCount() {
    return this.session.data && this.session.data[0].length;
  }

  @computed
  get openedCells() {
    return this.dataValues.filter((cell) => cell.isOpen);
  }

  @computed
  get virtualBombs() {
    const virtualMines = [];
    this.openedCells.forEach((cell) => {
      if (cell.hasValue && cell.value === cell.hiddenNeighbors.length) {
        cell.hiddenNeighbors.forEach((neighbor) => {
          if (!virtualMines.includes(neighbor)) {
            virtualMines.push(neighbor);
          }
        });
      }
    });
    return virtualMines;
  }

  @computed
  get safeCells() {
    const safeCells = [];
    this.openedCells.forEach((cell) => {
      if (cell.hasValue) {
        const virtualBombsCount = cell.hiddenNeighbors.filter((neighbor) =>
          this.virtualBombs.includes(neighbor)
        ).length;
        const allBombsFound = virtualBombsCount === cell.value;
        if (allBombsFound) {
          const safeCellsOfCell = cell.hiddenNeighbors.filter(
            (neighbor) => !this.virtualBombs.includes(neighbor)
          );
          safeCellsOfCell.forEach((cell) => safeCells.push(cell));
        }
      }
    });
    return safeCells;
  }

  @action
  makeDecision = (isForce) => {
    const left =
      this.rowCount * this.columnCount -
      this.openedCells.length -
      this.virtualBombs.length;

    console.log("safe:", this.safeCells.length, "left:", left);
    if (
      left < this.rowCount * this.columnCount * 0.1 &&
      this.safeCells.length === 0 &&
      !isForce
    ) {
      //this.isDecisionMakingStopped = true;
    }

    if (this.isDecisionMakingStopped) {
      return;
    }

    if (this.openedCells.length === 0) {
      this.session.openCell(
        Math.round(this.rowCount / 2),
        Math.round(this.columnCount / 2)
      );
    }
    if (this.safeCells.length > 0) {
      this.safeCells.forEach((safeCell) => {
        const cell = this.data.get(safeCell);
        this.session.openCell(cell.row, cell.column);
      });
    }
    if (this.safeCells.length === 0) {
      const hiddenCells = this.dataValues
        .filter((cell) => !cell.isOpen && !this.virtualBombs.includes(cell.key))
        .sort((a, b) => {
          return a.dangerousIndex - b.dangerousIndex;
        });
      if (hiddenCells.length > 0) {
        const nextCell = hiddenCells[0];
        this.session.openCell(nextCell.row, nextCell.column);
      }
    }

    this.decisionMadeSignal = {};
  };

  @action
  startMakingDecision = (isStarted) => {
    if (isStarted) {
      this.isDecisionMakingStopped = false;
      this.disposeMakeDecision = reaction(
        () => this.session.data,
        this.makeDecision,
        { delay: 1, fireImmediately: true }
      );
    }
  };

  @action
  stopMakingDecision = (isStopped) => {
    if (this.disposeMakeDecision) {
      this.disposeMakeDecision();
    }
  };
}
