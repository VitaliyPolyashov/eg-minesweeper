import { getHiddenNeighbors, getKey, getNeighbors } from "./utils";

export default class Cell {
  constructor(data, row, column) {
    this.data = data;
    this.row = row;
    this.column = column;
    this.value = data[row][column] != null ? Number(data[row][column]) : -3;
    this.hasValue = this.value > 0;
    this.isOpen = this.value >= 0;
  }

  get key() {
    return getKey(this.row, this.column);
  }

  get hiddenNeighbors() {
    return getHiddenNeighbors(this.data, this.row, this.column).map((cell) =>
      getKey(cell[0], cell[1])
    );
  }

  get dangerousIndex() {
    const dangerousIndex = getNeighbors(this.data, this.row, this.column)
      .filter((cell) => this.data[cell[0]][cell[1]] !== null)
      .reduce((acc, row) => {
        acc +=
          this.data[row[0]][row[1]] /
          getHiddenNeighbors(this.data, row[0], row[1]).length;
        return acc;
      }, 0);
    if (dangerousIndex === 0) {
      return 2;
    }
    return dangerousIndex;
  }
}
