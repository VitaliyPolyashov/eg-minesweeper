import { getHiddenNeighbors, getKey, getNeighbors } from "./utils";

test("getKeyTest", () => {
  expect(getKey(1, 2)).toBe("1|2");
});

test("getNeighborsTest", () => {
  expect(
    getNeighbors(
      [
        [null, null],
        [null, null],
      ],
      1,
      1
    )
  ).toStrictEqual([
    [0, 0],
    [1, 0],
    [0, 1],
  ]);
});

test("getHiddenNeighborsTest", () => {
  expect(
    getHiddenNeighbors(
      [
        [1, null],
        [null, null],
      ],
      1,
      1
    )
  ).toStrictEqual([
    [1, 0],
    [0, 1],
  ]);
});
