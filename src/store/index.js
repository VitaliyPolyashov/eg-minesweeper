import ApiCaller from "./ApiCaller";
import CallChainer from "./CallChainer";
import Session from "./Session";
import Sweeper from "./Sweeper";
import { action, computed } from "mobx";

export default class Store {
  constructor() {
    this.apiCaller = new ApiCaller("wss://hometask.eg1236.com/game1/");
    this.session = new Session(this.apiCaller);
    this.sweeper = new Sweeper(this.session, this.apiCaller);
    this.callChainer = new CallChainer(this.apiCaller, this.sweeper);
    this.levels = [1, 2, 3, 4];
  }

  start = (level) => {
    this.session.start(level);
  };

  @computed
  get isAutosolvingStopped() {
    return this.sweeper.isDecisionMakingStopped;
  }

  @action
  pauseAutosolving = () => {
    this.sweeper.isDecisionMakingStopped = true;
  };

  @action
  continueAutosolving = () => {
    this.sweeper.isDecisionMakingStopped = false;
    this.sweeper.makeDecision(true);
  };

  @action
  openManually = (row, column) => {
    this.session.openCell(row, column);
    this.apiCaller.getMap();
  };
}
