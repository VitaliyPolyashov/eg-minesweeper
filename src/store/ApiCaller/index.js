import { observable } from "mobx";

export default class ApiCaller {
  constructor(url) {
    this.webSocket = new WebSocket(url);
    this.webSocket.onmessage = this.parseMessage;
    this.webSocket.onopen = this.setOpenState;
  }

  setOpenState = () => {
    this.isOpened = true;
  };

  resultMapping = {
    new: (data) => (this.newResult = { data }),
    map: (data) => (this.mapResult = { data }),
    open: (data) => (this.openResult = { data }),
  };

  parseMessage = ({ data }) => {
    const parts = data.split(":");
    const header = parts[0];
    const assignResult = this.resultMapping[header];
    if (assignResult) {
      assignResult(parts[1]);
    }
  };

  @observable isOpened = false;
  @observable newResult;
  @observable mapResult;
  @observable openResult;

  start = (level) => {
    this.webSocket.send(`new ${level}`);
  };

  getMap = () => {
    this.webSocket.send("map");
  };

  open = (row, column) => {
    this.webSocket.send(`open ${column} ${row}`);
  };
}
