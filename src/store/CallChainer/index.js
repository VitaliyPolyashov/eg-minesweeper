import { reaction } from "mobx";

export default class CallChainer {
  constructor(apiCaller, sweeper) {
    reaction(() => sweeper.decisionMadeSignal, apiCaller.getMap);
    reaction(() => apiCaller.newResult, apiCaller.getMap);
  }
}
