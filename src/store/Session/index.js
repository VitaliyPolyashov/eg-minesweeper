import { action, computed, observable, reaction } from "mobx";

export default class Session {
  @observable level;
  @observable data;
  @observable isLosed = false;
  @observable isWon = false;

  constructor(apiCaller) {
    this.apiCaller = apiCaller;
    reaction(() => this.apiCaller.mapResult, this.setData);
    reaction(() => this.apiCaller.openResult, this.checkState);
  }

  @computed
  get isStarted() {
    return Boolean(this.data) && !this.isLosed && !this.isWon;
  }

  @action
  setData = ({ data }) => {
    const lines = data.split(/(?:\r\n|\r|\n)/g);
    const result = lines
      .filter((line) => line.length > 0)
      .map((line) => {
        return line.split("").map((cell) => {
          if (isFinite(cell)) {
            return Number(cell);
          }
          if (cell === "*") {
            return -1;
          }
          return null;
        });
      });
    this.data = result;
  };

  @action
  openCell = (row, cell) => {
    this.apiCaller.open(row, cell);
  };

  @action
  checkState = ({ data }) => {
    if (data === " You lose") {
      this.isLosed = true;
      //uncomment to autorestart level
      //setTimeout(() => this.start(this.level), 1000);
    }
    if (data.includes(" You win")) {
      this.isWon = true;
    }
  };

  @action
  start = (level) => {
    this.level = level;
    this.apiCaller.start(level);
    this.isWon = false;
    this.isLosed = false;
  };
}
