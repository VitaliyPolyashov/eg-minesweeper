import React from "react";
import styles from "./index.module.css";
import { observer } from "mobx-react";

const StartPanel = ({ onClick, levels }) => {
  return (
    <div className={styles.levelsContainer}>
      <span>Select level:</span>
      {levels.map((level, index) => {
        return (
          <button key={`l${index}`} onClick={() => onClick(level)}>
            {level}
          </button>
        );
      })}
    </div>
  );
};

export default observer(StartPanel);
