import React from "react";
import styles from "./index.module.css";
import Cell from "./Cell";
import { observer } from "mobx-react";

const Table = ({ data, onClick }) => {
  const onClickFunc = (e) => {
    onClick(e.target.dataset.row, e.target.dataset.column);
  };
  return (
    <div className={styles.table}>
      {data &&
        data.map((row, rowIndex) => {
          return (
            <div key={`r${rowIndex}`} className={styles.row}>
              {row.map((cell, columnIndex) => {
                return (
                  <Cell
                    key={`c${columnIndex}`}
                    value={cell}
                    row={rowIndex}
                    column={columnIndex}
                    onClick={onClickFunc}
                  />
                );
              })}
            </div>
          );
        })}
    </div>
  );
};

export default observer(Table);
