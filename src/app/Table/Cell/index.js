import React from "react";
import styles from "./index.module.css";
import classNames from "classnames";

const Cell = ({ value, onClick, row, column }) => {
  const noValue = value === null;
  return (
    <div
      className={classNames(styles.cell, { [styles.hidden]: noValue })}
      onClick={onClick}
      data-row={row}
      data-column={column}
    >
      {value >= 0 && <span>{value}</span>}
      {value === -1 && <span>*</span>}
      {value === null && <span></span>}
    </div>
  );
};

export default Cell;
