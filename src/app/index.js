import React from "react";
import styles from "./index.module.css";
import { inject, observer } from "mobx-react";
import Table from "./Table";
import StartPanel from "./StartPanel";

const App = ({ Store }) => {
  return (
    <div className={styles.app}>
      {Store.session && (
        <div className={styles.container}>
          <Table data={Store.session.data} onClick={Store.openManually} />
        </div>
      )}
      {Store.session.isLosed && <div>You losed. Start Again. </div>}
      {Store.session.isWon && <div>You won!</div>}
      {Store.session.isStarted && !Store.isAutosolvingStopped && (
        <button onClick={Store.pauseAutosolving}>Pause Auto Mode</button>
      )}
      {Store.session.isStarted && Store.isAutosolvingStopped && (
        <button onClick={Store.continueAutosolving}>Continue Auto Mode</button>
      )}
      {!Store.session.isStarted && (
        <StartPanel levels={Store.levels} onClick={Store.start} />
      )}
    </div>
  );
};

export default inject("Store")(observer(App));
