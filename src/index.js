import React from "react";
import ReactDOM from "react-dom";
import App from "./app";
import Store from "./store";
import { Provider } from "mobx-react";

const store = new Store();
ReactDOM.render(
  <Provider Store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
