This is Minesweeper autosolver
You can run it in 1-4 modes. You can pause execution and open cells by yourself.
You will be notified when you losed or won and be able to start over.

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

Please run `npm start` command to start app.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Please run `npm test` command to run units test. There is no full test coverage. They are created just for test task purposes.
